package com.nmeo.packageme;

/**
 * The Product class represents a product that extends the Article class.
 * It has additional attributes and methods specific to a product.
 */
public class Product extends Article {
    private int mCount; // The count of available products

    /**
     * Constructor to create a Product object with the given parameters.
     *
     * @param pName        The name of the product.
     * @param pDescription The description of the product.
     * @param pPrice       The price of the product.
     * @param pCount       The count of available products.
     */
    public Product(String pName, String pDescription, double pPrice, int pCount) {
        super(pName, pDescription, pPrice);
        mCount = pCount;
    }

    /**
     * Copy constructor to create a Product object by copying the attributes
     * of another Product object.
     *
     * @param pProductToCopy The Product object to copy.
     */
    public Product(Product pProductToCopy) {
        // We can access the pProductToCopy's private attributes
        // because we are inside the Product class :)
        this(pProductToCopy.mName, pProductToCopy.mDescription, pProductToCopy.mPrice, pProductToCopy.mCount);
        this.mUuid = pProductToCopy.mUuid;
    }

    /**
     * Gets the count of available products.
     *
     * @return The count of available products.
     */
    public int getCount() {
        return mCount;
    }

    /**
     * Increments the count of available products by the given quantity.
     *
     * @param pQuantity The quantity to increment.
     */
    public void incrementCount(int pQuantity) {
        mCount += pQuantity;
    }

    /**
     * Decrements the count of available products by 1.
     */
    public void decrementCount() {
        if (mCount > 0) {
            mCount--;
        }
    }

    /**
     * Returns a string representation of the product with the count of available products.
     *
     * @return A string representation of the product with the count of available products.
     */
    public String printWithCount() {
        return String.format("%s\n  -%s\n  -%.2f$\n  -%d available", mName, mDescription, mPrice, mCount);
    }

    /**
     * Overrides the toString() method to return a string representation of the product.
     *
     * @return A string representation of the product.
     */
    @Override
    public String toString() {
        return String.format("%s\n  -%s\n  -%.2f$", mName, mDescription, mPrice);
    }
}
