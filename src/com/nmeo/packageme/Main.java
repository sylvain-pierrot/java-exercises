package com.nmeo.packageme;

import com.nmeo.packageme.ProductAdvertised;
import com.nmeo.packageme.ServiceAdvertised;
import com.nmeo.packageme.OnlineShop;
import com.nmeo.packageme.OnlineShopCli;

/**
 * The Main class represents the entry point of the program.
 * It creates an instance of the OnlineShop and adds products and services to it.
 * It also creates an instance of OnlineShopCli and runs the command-line interface.
 */
public class Main {
    /**
     * The main method is the entry point of the program.
     * It creates an instance of OnlineShop, adds products and services to it,
     * creates an instance of OnlineShopCli and runs the command-line interface.
     *
     * @param args Command-line arguments (not used in this program).
     */
    public static void main(String[] args) {
        // Create an instance of OnlineShop with capacity for 10 products and 10 services
        OnlineShop onlineShop = new OnlineShop(10, 10);

        // Add normal products
        onlineShop.addProduct("Hoover", "Cleans the floor", 150, 10);
        onlineShop.addProduct("Karcher", "Cleans anything with water", 250, 15);
        onlineShop.addProduct("Screen", "Displays interesting stuff", 200, 4);

        // Add services
        onlineShop.addService("Streaming video", "Entertainment!", 1);
        onlineShop.addService("Streaming audio", "Let the music play", 2);

        // Add underlined products
        ProductAdvertised newProduct = new ProductAdvertised("PS5", "Run video games", 200, 4);
        onlineShop.addProduct(newProduct);

        ServiceAdvertised newService = new ServiceAdvertised("HBO", "Get me to watch GoT", 20, 1);
        onlineShop.addService(newService);

        // Create an instance of OnlineShopCli with the OnlineShop object
        OnlineShopCli onlineShopCli = new OnlineShopCli(onlineShop);

        // Run the command-line interface
        onlineShopCli.runCli();
    }
}
