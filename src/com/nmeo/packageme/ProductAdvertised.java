package com.nmeo.packageme;

import com.nmeo.packageme.Constants;

/**
 * The ProductAdvertised class represents an advertised product that extends the Product class.
 * It has additional methods specific to an advertised product.
 */
public class ProductAdvertised extends Product {

    /**
     * Constructor to create a ProductAdvertised object with the given parameters.
     *
     * @param pName        The name of the advertised product.
     * @param pDescription The description of the advertised product.
     * @param pPrice       The price of the advertised product.
     * @param pCount       The count of available advertised products.
     */
    public ProductAdvertised(String pName, String pDescription, double pPrice, int pCount) {
        super(pName, pDescription, pPrice, pCount);
    }

    /**
     * Returns a string representation of the advertised product with the count of available products.
     *
     * @return A string representation of the advertised product with the count of available products.
     */
    public String printWithCount() {
        return String.format("\n%s\n%s\n  -%s\n  -%.2f$\n  -%d available\n%s\n",
            Constants.SEPARATOR,
            mName,
            mDescription,
            mPrice,
            getCount(),
            Constants.SEPARATOR);
    }

    /**
     * Overrides the toString() method to return a string representation of the advertised product.
     *
     * @return A string representation of the advertised product.
     */
    @Override
    public String toString() {
        return String.format("\n%s\n%s\n  -%s\n  -%.2f$\n%s\n",
            Constants.SEPARATOR,
            mName,
            mDescription,
            mPrice,
            Constants.SEPARATOR);
    }
}
