package com.nmeo.packageme;
import java.util.ArrayList;
import java.util.Optional;
import java.util.UUID;

import com.nmeo.packageme.Constants;
import com.nmeo.packageme.Product;
import com.nmeo.packageme.Service;

public class OnlineShop implements IOnlineShop {
    private ArrayList<Product> mProducts;
    private int mMaxProductCount;
    private ArrayList<Service> mServices;
    private int mMaxServicesCount;
    private Basket mBasket;
    private final int BASKET_SIZE = 10;

    public OnlineShop(int pMaxProduct, int pMaxServices) {
        mProducts = new ArrayList<Product>();
        mMaxProductCount = pMaxProduct;
        mServices = new ArrayList<Service>();
        mMaxServicesCount = pMaxServices;
        mBasket = new Basket(BASKET_SIZE);
    }

    public void addProduct(String pName, String pDescription, double pPrice, int pStock) {
        Product newProduct = new Product(pName, pDescription, pPrice, pStock);
        addProduct(newProduct);
    }

    public void addProduct(Product pProduct) {
        if(mProducts.size() < mMaxProductCount) {
            mProducts.add(pProduct);
        } else {
            System.err.println(Constants.ANSI_RED + "The online shop product section is full" + Constants.ANSI_RESET);
        }
    }

    public void addService(String pName, String pDescription, double pPricePerHour) {
        Service newService = new Service(pName, pDescription, pPricePerHour, 0);
        addService(newService);
    }

    public void addService(Service pService) {
        if(mServices.size() < mMaxServicesCount) {
            mServices.add(pService);
        } else {
            System.err.println(Constants.ANSI_RED + "The online shop service section is full" + Constants.ANSI_RESET);
        }
    }

    public void restockProduct(int pIndex, int pQuantity) {
        if(pIndex > mMaxProductCount || pIndex > mProducts.size()) {
            System.err.println(Constants.ANSI_RED + "Invalid index" + Constants.ANSI_RESET);
            return;
        }
        mProducts.get(pIndex).incrementCount(pQuantity);
        System.err.println(Constants.ANSI_GREEN + String.format("The product %s's quantity has been increased by %d", mProducts.get(pIndex).getName(), pQuantity) + Constants.ANSI_RESET);
    }

    public void displayBasketContent() {
        mBasket.displayArticles();
    }

    public void displayProductsList() {
        System.out.println("Please find below our products");
        for(int i = 0; i< mProducts.size(); i++) {
            System.out.println(String.format("%d-%s", i, mProducts.get(i).printWithCount()));
        }
    }

    public void displayServicesList() {
        System.out.println("Please find below our services");
        for(int i = 0; i< mServices.size(); i++) {
            System.out.println(String.format("%d-%s", i, mServices.get(i).toString()));
        }
    }

    public void addProductToBasket(int pIndex) {
        if(pIndex > mMaxProductCount || pIndex > mProducts.size()) {
            System.err.println(Constants.ANSI_RED + "Invalid index" + Constants.ANSI_RESET);
            return;
        }
        if(mProducts.get(pIndex).getCount() <= 0) {
            System.err.println(Constants.ANSI_RED + "Product out of stock" + Constants.ANSI_RESET);
            return;
        }

        System.out.println(Constants.ANSI_GREEN + String.format("Article %s added to basket", mProducts.get(pIndex).getName())+ Constants.ANSI_RESET);
        // We choose to create a copy of the product here. Indeed, Java is sending parameters by reference.
        // If we do not create a copy at this point, we will be sending a reference of the product in
        // the online store into our basket. Then if basket makes any changes on the article, my online store
        // will be impacted.
        Product myCopyProduct = new Product(mProducts.get(pIndex));
        // This line uses polymorphisme and casts the product into an article to send it to the basket
        if(mBasket.addArticle(myCopyProduct)) {
            mProducts.get(pIndex).decrementCount();
        }
    }

    public void addServiceToBasket(int pIndex, int pDurationH) {
        if(pIndex > mMaxServicesCount || pIndex > mServices.size()) {
            System.err.println(Constants.ANSI_RED + "Invalid index" + Constants.ANSI_RESET);
            return;
        }

        System.out.println(Constants.ANSI_GREEN + String.format("Service %s added to basket", mServices.get(pIndex).getName()) + Constants.ANSI_RESET);
        // We encounter the same cas as bedore here, we need a copy of the service isntance. In addition
        // we need to compute the price according to the duration selected
        Service myCopyService = new Service(mServices.get(pIndex));
        myCopyService.setDurationH(pDurationH);
        myCopyService.computePrice();
        // This line uses polymorphisme and casts the service into an article to send it to the basket
        mBasket.addArticle(myCopyService);
    }

    public void removeArticalFromBasket(int pIndex) {
        Optional<UUID> optArticleId = mBasket.removeArticle(pIndex);
        if(optArticleId.isPresent()) {
            Optional<Product> productRemovedFromBasket =  mProducts.stream()
                .filter(article -> optArticleId.get().equals(article.getUuid()))
                .findFirst();
            if(productRemovedFromBasket.isPresent()) {
                productRemovedFromBasket.get().incrementCount(1);
            }
        }
    }

    public void purchaseItems() {
        System.out.println(Constants.ANSI_GREEN + String.format("Thank you for your purchase, %.2f$ has been paid", mBasket.basketPrice()) + Constants.ANSI_RESET);
        mBasket.removeAllArticle();
        System.out.println(Constants.ANSI_GREEN + String.format("Your basket has been emptied") + Constants.ANSI_RESET);

    }
}
