package com.nmeo.theonlineshop;
import java.util.Scanner;

public class OnlineShopCli {
    private enum CliState {
        HOME, // HomeState
        PRODUCT_VIEW, // ProductViewState
        SERVICE_VIEW, // ServiceViewState
        BASKET_VIEW, // BasketViewState
        PURCHASE_VIEW, // PurchaseViewState
        RESTOCK_VIEW, // RestockViewState
        ADD_PRODUCT_SERVICE_VIEW //AddProductServiceViewState
    }

    private IOnlineShop mOnlineShop;
    private CliState mCliState;
    private static final int MAIN_MENU_CODE = -1;

    public OnlineShopCli(IOnlineShop pOnlineShop) {
        mOnlineShop = pOnlineShop;
        mCliState = CliState.HOME;
    }

    private void printHomeInterface() {
        System.out.println(Constants.ANSI_PURPLE + "Welcome to the online shop");
        System.out.println(" 1- Browse products");
        System.out.println(" 2- Browse services");
        System.out.println(" 3- My Basket");
        System.out.println(" 4- Purchase items");
        System.out.println(" 5- Restock an existing product");
        System.out.println(" 6- Add new products/services");
        System.out.println("Please enter a number to navigate to the next menu" + Constants.ANSI_RESET);
    }

    private void printProductViewInterface() {
        mOnlineShop.displayProductsList();
        System.out.println(String.format("%d- Return to home menu", MAIN_MENU_CODE));
        System.out.println("Choose the product's index you would like to add in your basket");
    }

    private void printServiceViewInterface() {
        mOnlineShop.displayServicesList();
        System.out.println(String.format("%d- Return to home menu", MAIN_MENU_CODE));
        System.out.println("Choose the service's index you would like to add in your basket");
    }

    private void printBasketViewInterface() {
        mOnlineShop.displayBasketContent();
        System.out.println("If you want to delete an item, please enter the index accordingly.");
        System.out.println(String.format("Otherwise enter %d to return to main menu", MAIN_MENU_CODE));
    }

    private void printRestockViewInterface() {
        mOnlineShop.displayProductsList();
        System.out.println("If you want to restock an item, please enter the index accordingly.");
        System.out.println(String.format("Otherwise enter %d to return to main menu", MAIN_MENU_CODE));
    }

    private void computeHomeInput(int pUserInput) {
        switch(pUserInput) {
            case 1:
                mCliState = CliState.PRODUCT_VIEW;
                break;
            case 2:
                mCliState = CliState.SERVICE_VIEW;
                break;
            case 3:
                mCliState = CliState.BASKET_VIEW;
                break;
            case 4:
                mCliState = CliState.PURCHASE_VIEW;
                break;
            case 5:
                mCliState = CliState.RESTOCK_VIEW;
                break;
            case 6:
                mCliState = CliState.ADD_PRODUCT_SERVICE_VIEW;
                break;
            default:
                System.err.println(Constants.ANSI_RED + "Unsupported input" + Constants.ANSI_RESET);
                break;
        }
    }

    private void computeProductViewInput(int pIndex) {
        if(pIndex == MAIN_MENU_CODE) {
            mCliState = CliState.HOME;
        } else {
            mOnlineShop.addProductToBasket(pIndex);
        }
    }

    private void computeServiceViewInput(int pIndex, int pDurationH) {
            mOnlineShop.addServiceToBasket(pIndex, pDurationH);
    }

    private void computeBasketViewInput(int pIndex) {
        if(pIndex == MAIN_MENU_CODE) {
            mCliState = CliState.HOME;
        } else {
            mOnlineShop.removeArticalFromBasket(pIndex);
        }
    }

    private void computeRestockViewInput(int pIndex, int pQuantity) {
        mOnlineShop.restockProduct(pIndex, pQuantity);
    }

    private void computeAddProductInput(Scanner scanner) {
        scanner.nextLine();
        System.out.println("Please enter the product's name");
        String name = scanner.nextLine();
        System.out.println("Please enter the product's description");
        String description = scanner.nextLine();
        System.out.println("Please enter the product's price");
        double price = scanner.nextDouble();
        System.out.println("Please enter the product's quantity");
        int count = scanner.nextInt();
        mOnlineShop.addProduct(name, description, price, count);
    }

    private void computeAddServiceViewInput(Scanner scanner) {
        scanner.nextLine();
        System.out.println("Please enter the service's name");
        String name = scanner.nextLine();
        System.out.println("Please enter the service's description");
        String description = scanner.nextLine();
        System.out.println("Please enter the service's price per hour");
        double pricePerHour = scanner.nextDouble();
        mOnlineShop.addService(name, description, pricePerHour);
    }

    public void runCli() {
        Scanner scanner = new Scanner(System.in);
        int userInput = 0;
        while(true) {
            System.out.println("*******************************************************************************");
            // Print interface switch
            switch (mCliState) {
                case HOME:
                    printHomeInterface();
                    userInput = scanner.nextInt();
                    break;
                case PRODUCT_VIEW:
                    printProductViewInterface();
                    userInput = scanner.nextInt();
                    break;
                case SERVICE_VIEW:
                    printServiceViewInterface();
                    userInput = scanner.nextInt();
                    break;
                case BASKET_VIEW:
                    printBasketViewInterface();
                    userInput = scanner.nextInt();
                    break;
                case PURCHASE_VIEW:
                    System.out.println("Thank you for your purchase !");
                    break;
                case RESTOCK_VIEW:
                    printRestockViewInterface();
                    userInput = scanner.nextInt();
                    break;
                case ADD_PRODUCT_SERVICE_VIEW:
                    System.out.println("Would you like to restock\n 1- Products\n 2- Services \n-1- Return to main menu");
                    userInput = scanner.nextInt();
                    break;
                default:
                    System.err.println(Constants.ANSI_RED + "Unsupported state" + Constants.ANSI_RESET);
                    break;
            }
            // Compute response switch
            switch (mCliState) {
                case HOME:
                    computeHomeInput(userInput);
                    break;
                case PRODUCT_VIEW:
                    computeProductViewInput(userInput);
                    break;
                case SERVICE_VIEW:
                    if(userInput == MAIN_MENU_CODE) {
                        mCliState = CliState.HOME;
                    } else {
                        System.out.println("For how long (in hour) do you require this service ? ");
                        int durationH = scanner.nextInt();
                        computeServiceViewInput(userInput, durationH);
                    }
                    break;
                case BASKET_VIEW:
                    computeBasketViewInput(userInput);
                    break;
                case PURCHASE_VIEW:
                    mOnlineShop.purchaseItems();
                    mCliState = CliState.HOME;
                    break;
                case RESTOCK_VIEW:
                    if(userInput == MAIN_MENU_CODE) {
                        mCliState = CliState.HOME;
                    } else {
                        System.out.println("How many products would you like to add ?");
                        int quantity = scanner.nextInt();
                        computeRestockViewInput(userInput, quantity);
                    }
                    break;
                case ADD_PRODUCT_SERVICE_VIEW:
                    if(userInput == MAIN_MENU_CODE) {
                        mCliState = CliState.HOME;
                    } else if (userInput == 1) {
                        // Scanner is passed by reference here, the same object will be used
                        computeAddProductInput(scanner);
                    } else if (userInput == 2) {
                        // Scanner is passed by reference here, the same object will be used
                        computeAddServiceViewInput(scanner);
                    } else {
                        System.err.println(Constants.ANSI_RED + "Unsupported index" + Constants.ANSI_RESET);
                    }
                    break;
                default:
                    System.err.println(Constants.ANSI_RED + "Unsupported state" + Constants.ANSI_RESET);
                    break;
            }
        }
    }
}
