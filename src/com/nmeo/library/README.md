# The library exercise

In this exercise, you'll training on exceptions, collections, streams and multithreading

# Handle exceptions

In the `LibraryCsvParser.java` file, handle the exceptions to be able to parse the file despite of the dataset errors.

# Store your books

In the `SafeLibrary.java` file, create a collection (you'll have to choose the one you think is the best for this use case) and store your books by filling the `addBook` and the `getLibrarySize` method !
In this exercice we only want to store each book once (we don't handle book count). 

# Start your renting business !

In the `dataset` directory, you'll find 4 `borrowerX.txt` files. Each file contains a title list the user wants to borrow. Create 4 threads to borrow all this books faster !
Make sure your `SafeLibrary` remains safe !
Use streams to make sure the books you want to borrow are in your library !

# The destroyer enters

In the `dataset` directory, you'll find a `destroy.txt` file. These books are outdated, and you'll have to remove them from the library. This task has to be handle in a new thread running concurrently with the 4 threads we created before !
Fill the methods `getBorrowedBooksCount` and `getInLibraryBooksCount` to report the statistics !

# ReadWrite lock to the rescue

At this point we've got a thread safe program but we received some complains from the users : the application is too slow ! We'll have to improve the performance !
Check out [this link](https://docs.oracle.com/javase/8/docs/api/java/util/concurrent/locks/ReadWriteLock.html) and make some improvement to your SafeLibrary !